package hello.managers;

import hello.models.Position;
import hello.models.User;

import java.util.List;

/**
 * Created by dmsh0714 on 05.04.2017.
 */
public interface UserManager {
    void create(User user);
    void delete(int id);
    void update(User user);
    List<User> getUsers();
    List<User> getUserByPosition(Position postion);
    User getUserById(int id);
}
