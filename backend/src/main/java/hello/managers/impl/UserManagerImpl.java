package hello.managers.impl;

import hello.dao.PositionDAO;
import hello.dao.UserDAO;
import hello.managers.UserManager;
import hello.models.Position;
import hello.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

/**
 * Created by dmsh0714 on 05.04.2017.
 */
@Service
public class UserManagerImpl implements UserManager {

    @Autowired
    private UserDAO userDao;

    @Transactional
    @Override
    public void create(User user) {
        userDao.create(user);
    }

    @Transactional
    @Override
    public void delete(int id) {
        User user = userDao.getUserById(id);
        userDao.delete(user);
    }

    @Transactional
    @Override
    public void update(User user) {
        userDao.update(user);
    }

    @Transactional
    @Override
    public List<User> getUsers() {
        return userDao.getUsers();
    }

    @Transactional
    @Override
    public List<User> getUserByPosition(Position postion) {
        return userDao.getUserByPosition(postion.getId());
    }

    @Transactional
    @Override
    public User getUserById(int id) {
        return userDao.getUserById(id);
    }
}
