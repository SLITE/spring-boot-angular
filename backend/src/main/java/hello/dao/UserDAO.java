package hello.dao;

import hello.models.User;

import java.util.List;

/**
 * Created by dmsh0714 on 05.04.2017.
 */
public interface UserDAO {
    void create(User user);
    void delete(User user);
    void update(User user);
    List<User> getUsers();
    List<User> getUserByPosition(int postion);
    User getUserById(int id);
}
