package hello.dao.impl;

import hello.dao.PositionDAO;
import hello.models.Position;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by dmsh0714 on 04.04.2017.
 */
@Service
public class PositionDAOImpl implements PositionDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Position> getAll() {
        Session session = sessionFactory.openSession();
        List<Position> listOfPositions =  session.createCriteria(Position.class).list();
        session.close();
        return listOfPositions;
    }

    @Override
    public void addPosition(Position position) {
        Session session = sessionFactory.openSession();
        session.save(position);
        session.close();
    }

    @Override
    public void deletePosition(Integer id) {
        Session session = sessionFactory.openSession();
        session.delete(getPositionById(id));
        session.close();
    }

    @Override
    public Position getPositionById(Integer id) {
        Session session = sessionFactory.openSession();
        Position position = session.get(Position.class,id);
        session.close();
        return position;
    }
}
