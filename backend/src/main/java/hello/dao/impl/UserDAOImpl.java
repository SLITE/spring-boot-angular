package hello.dao.impl;

import hello.dao.UserDAO;
import hello.models.User;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Created by dmsh0714 on 05.04.2017.
 */
@Service
public class UserDAOImpl implements UserDAO {


    private static String POSITION_FIELD_NAME = "position";
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void create(User user) {
        Session session = sessionFactory.openSession();
        session.save(user);
        session.close();
    }

    @Override
    public void delete(User user) {
        Session session = sessionFactory.openSession();
        session.delete(user);
        session.close();
    }

    @Override
    public void update(User user) {
        Session session = sessionFactory.openSession();
        session.update(user);
        session.close();
    }

    @Override
    public List<User> getUsers() {
        Session session = sessionFactory.openSession();
        List<User> users = session.createCriteria(User.class).list();
        session.close();
        return users;
    }

    @Override
    public List<User> getUserByPosition(int postion) {
        Session session = sessionFactory.openSession();
        Criteria usersCriteria = session.createCriteria(User.class);
        List<User> users = usersCriteria.add(Restrictions.eq(POSITION_FIELD_NAME, postion)).list();
        session.close();
        return users;
    }

    @Override
    public User getUserById(int id) {
        Session session = sessionFactory.openSession();
        User user = session.get(User.class,id);
        session.close();
        return user;
    }
}
