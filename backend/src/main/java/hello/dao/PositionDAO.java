package hello.dao;

import hello.models.Position;

import java.util.List;

/**
 * Created by dmsh0714 on 04.04.2017.
 */
public interface PositionDAO {
    List<Position> getAll();
    void addPosition(Position position);
    void deletePosition(Integer id);
    Position getPositionById(Integer id);
}
