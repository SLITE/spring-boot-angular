package hello.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import hello.managers.PositionsManager;
import hello.models.Position;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by dmsh0714 on 04.04.2017.
 */
@RestController
public class PositionController {

    @Autowired
    private PositionsManager positionsManager;

    @RequestMapping("/api/createPosition/{name}")
    @ResponseBody
    public String create(@PathVariable String name) {
        try{
            positionsManager.createPosition(name);
        }
        catch (Exception e){
            return e.getMessage();
        }
        return Boolean.TRUE.toString();
    }

    @RequestMapping("/api/deletePosition/{id}")
    @ResponseBody
    public String delete(@PathVariable int id) {
        try{
            positionsManager.deletePosition(id);
        }
        catch (Exception e){
            return e.getMessage();
        }
        return Boolean.TRUE.toString();
    }

    @RequestMapping("/api/getPosition")
    @ResponseBody
    public String get() throws JsonProcessingException {
        List<Position> pos = positionsManager.getAll();
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(pos);
    }

    @RequestMapping("/api/getPosition/{id}")
    @ResponseBody
    public String get(@PathVariable int id) throws JsonProcessingException {
        Position pos = positionsManager.getById(id);
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(pos);
    }
}
