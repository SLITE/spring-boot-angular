package hello.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import hello.managers.PositionsManager;
import hello.managers.UserManager;
import hello.models.*;


import hello.models.ui.UserUI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.fasterxml.jackson.databind.ObjectMapper;


import java.util.*;

/**
 * Created by dmsh0714 on 06.07.2016.
 */
@Controller
public class UserController {
    @Autowired
    private UserManager userManager;

    @Autowired
    private PositionsManager positionsManager;

    private ObjectMapper mapper = new ObjectMapper();

    @RequestMapping("/api/createUser")
    @ResponseBody
    public String create(@RequestBody UserUI userUI) throws JsonProcessingException {
        System.out.println("Login");
        System.out.println(mapper.writeValueAsString(userUI));
        try {
            User user = new User();
            user.setLogin(userUI.getLogin());
            user.setName(userUI.getName());
            user.setPosition(positionsManager.getById(userUI.getPositionId()));
            user.setPassword(userUI.getPassword());
            userManager.create(user);
        }
        catch (Exception ex) {
            return "Error creating the user: " + ex.toString();
        }
        return "User succesfully created!";
    }

    @RequestMapping("/api/updateUser")
    @ResponseBody
    public String update(User user) {
        try {
            userManager.create(user);
        }
        catch (Exception ex) {
            return "Error updating the user: " + ex.toString();
        }
        return "User succesfully updated!";
    }

    @RequestMapping(value = "/api/getUser/{id}",method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<User> getById(@PathVariable int id) throws JsonProcessingException {
        User user;
        try {
             user = userManager.getUserById(id);
        }
        catch (Exception ex) {
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<User>(user,HttpStatus.OK);
    }


    @RequestMapping("/api/getUsers")
    @ResponseBody
    public String getUsers() throws JsonProcessingException {
        List<User> users;
        try {
            users = userManager.getUsers();
        }
        catch (Exception ex) {
            return "User not found";
        }
        return mapper.writeValueAsString(users);
    }

    /*@RequestMapping("/get-task")
    @ResponseBody
    @PreAuthorize("hasRole('ROLE_USER')")
    public String getTask(long id) throws JsonProcessingException {

        Set<Project> tasks;
        try {
            User user = userDao.findOne(id);
            //tasks = user.getProjects();
        }
        catch (Exception ex) {
            return "User not found";
        }
        String res = "Empty";

        ObjectMapper mapper = new ObjectMapper();
        try {
           res += mapper.writeValueAsString(tasks.iterator().next());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return mapper.writeValueAsString(tasks);
    }                                             */

    @RequestMapping("/api/get-pass")
    @ResponseBody
    @PreAuthorize("hasRole('ROLE_USER')")
    public String getPass(){
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        String hashedPassword = passwordEncoder.encode("123456");
        boolean isTrue =passwordEncoder.matches("123456",hashedPassword);
        return String.valueOf(isTrue);
    }

    @RequestMapping("/api/delete")
    @ResponseBody
    public String delete(int id) {
        try {
            userManager.delete(id);
        }
        catch (Exception ex) {
            return "Error deleting the user: " + ex.toString();
        }
        return "User succesfully deleted!";
    }
}
